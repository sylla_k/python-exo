#L : perte de trensmission , m : longueur , n: largeur ,t: temps 
def elder_age(m,n,l,t):
    #je recupere la max et min entre m et n 
    m, n = max(m, n), min(m, n)
    #je converti en binaire la variable m puis je recupaire ca longueur que je sourtrait par 3 
    k = len(bin(m)) - 3
    # Cette opration revien a faire 2 * k * k
    z = 2 ** k
    # est la variable qui recupere le min entre Z et n 
    a = min(z, n)
    #je verifie les condition pour chaque cas 
    if l < z:
        var = ((a*(z-l-1)*(z-l)) // 2) % t
    else:
        var = 0

    if m > z:
        var = var + max(z-l, 0) * (m-z) * a % t
        var = var + elder_age(m-z, a, max(0, l-z), t)
    
    if n > z:
        var = var + max(z-l, 0) * (n-z) * a % t
        var = var + elder_age(z, n-z, max(0, l-z), t)
        var = var + elder_age(m-z, n-z, l, t)
    #Je retourne le reste de la division 
    return var % t