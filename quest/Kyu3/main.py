#fonction qui valid le perimetre d'un bateau retourn True si bon sinon False
def perimeter(field, horizontal, Vertical,direction, validCells):
    # Je verifie d'abord le cote gauche puis droit 
    for i in range(horizontal - 1, horizontal + direction[0] + 1):
        if 0 <= i < 10:
            j = Vertical - 1

            if j >= 0 and not validCells[i*10 + j] and field[i][j]:
                return False
            j = Vertical + direction[1]
            if j < 10 and not validCells[i*10 + j] and field[i][j]:
                return False

    # Puis le perimetre du bas 
    for j in range(Vertical, Vertical + direction[1]):
        if i < 10:
            i = horizontal + direction[0]
            if field[i][j]:
                return False

    return True


def get_direction(field, Horizontal, Vertical):
    i = 0 # variable qui retourne le nombre de case vers le bas 
    j = 0  # variable qui retourne le nombre de case vers la droite
    while Horizontal + i < 10 and field[Horizontal+i][Vertical]:
        i += 1
    while Vertical + j < 10 and field[Horizontal][Vertical+j]:
        j += 1
    #Je retourne le nombre case selon le sens 
    return i, j


def validate_battlefield(field):
    Taille_ship = {1: 4,  2: 3,3: 2, 4: 1}
    validCells = [False for _ in range(10*10)]
    # je retourne Faux si toute les cellule du tableau field est different de 20
    if sum(sum(cell for cell in row) for row in field) != 20:
        return False

    for i in range(len(field)):
        for j in range(len(field)):
            if not validCells[i*10 + j] and field[i][j]:
                ship_dir = get_direction(field, i, j)
                ship_len = max(ship_dir)
                
                if ship_len not in Taille_ship or Taille_ship[ship_len] == 0:
                    return False  # Si la longueur n'est pas compris dans les taille ou elle est egale a 0 retourne false 
                if sum(ship_dir) != ship_len + 1:
                    return False  # Si la somme  est superieur a longueur max +1
                if not perimeter(field, i, j, ship_dir, validCells):
                    return False  # Si certaint des bateau se touche je retourne false 
                #je parcour toute les case et je verifie ma condition 
                for x in range(j, j + ship_dir[1]):
                #
                    validCells[i*10 + x] = True
                for y in range(i, i + ship_dir[0]):
                    validCells[y*10 + j] = True

                Taille_ship[ship_len] -= 1
            else:
                validCells[i*10 + j] = True

    return True
	
