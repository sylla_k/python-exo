def valid_solution(board):
    for i in range(len(board)):
        hadd = 0
        vadd = 0
        for j in range(len(board)):
            #je parcour horizontalemnt en ajoutent les valeur
            vadd += board[j][i]
            #je parcour verticalement en ajoutant les valeur 
            hadd += board[i][j]
            #je verifi Qu il soit bien compris entre 1 et 9
            if board[i][j] < 1 or board[i][j] > 9:
                return False
        #Si le total de variable had ou  vadd et different de 45 je retourne faux
        if vadd != 45 or hadd != 45:
            return False

    #je verifie Block par block de longeur 3 et de largeur 3 
    for i in range(3):
        for j in range(3):
            gadd = 0
            for k in range(3):
                for l in range(3):
                    gadd += board[i*3+k][j*3+l]
                    #Si le nombre est different n'est pas compris entre 1 et 9 je retourne faux
                    if board[i][j] < 1 or board[i][j] > 9:
                        
                        return False
            #Si le le total des nombre  est different de 45 je retourne false 
            if gadd != 45:
                return False

    return True 
