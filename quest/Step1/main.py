#La fonction devrait renvoyer le résultat des nombres après avoir appliqué l'opération choisie.
def basic_op(operator, value1, value2):

    #Je teste si l'operenteur saisi est egale a l'un des 4 operateur logique
    if operator == '+': 
        
        #J'effectue une operation avec l'operateur correspondant et je retourne le resultat  
        return value1 + value2
    elif operator == '-':
        return value1 - value2
    elif operator == '*':
        return value1 * value2
    elif operator == '/':
        return value1 / value2