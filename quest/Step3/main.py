#La fonction a pour but de remplir un tableau avec tous les nombres jusqu'à ce nombre inclus, mais à l'exclusion de zéro
def monkey_count(n):
    #je definie un tableau vide 
    list = []
    #je cree une boucle for qui debute a 1 
    for i in range(1,n+1):
        #a chaque tour j'ajoute un element i a mon tableau
        list.append(i)
    #je retourne le tableau list 
    return list