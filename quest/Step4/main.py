#fonction qui suprime les voyelle 
def shortcut( s ):
    #je cree une liste qui contiendras toute mes voyelle 
    vowels = ('a', 'e', 'i', 'o', 'u')
    #Pour toute lettre contenu dans la chaine de caractere continuer la boucle 
    for x in s:
        #si la lettre fait partit des element de list vowels
        if x in vowels:
            #je remplace la lettre par rien je la suprime
            s = s.replace(x,"")
    #je retourne la chaine de caractere modifier        
    return(s)