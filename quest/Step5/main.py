#fonction qui met en carré chaque chiffre d'un nombre etles concaténe.
def square_digits(num):
    res = ''
    #je converti ma variable en chaine de caractere 
    num = str(num)
    for i in range(0,len(num)):
        #je multiplie le nombre par lui memme toute et je converti en entier 
        cal = int(num[i]) * int(num[i]) 
        #j'utilise l'operateur logique pour effectuer la concatenation
        res = res + str(cal)
    #je retourne que la parti entiere de la variable res
    return int(res)
