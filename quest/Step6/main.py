#La fonction doit renvoyer true si un triangle peut être construit avec les côtés de longueur donnée et false dans tous les autres cas.
def is_triangle(a, b, c):
    #je verifie si les 3 coter aditionner sont superrieur au plus grand coter multiplier par 2 si oui je retourne true
    if a + b + c > 2 * max(a, b, c):
         return True
         #sinon je retourne faux
    else :
        return False
   