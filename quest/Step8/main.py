def solution(n):
    #demo(explication) de valeurs chiffres en romain
    roman_numeral_dict = {
        0: ["I", "II", "III", "IV", "V","VI","VII", "VIII", "IX"], 
        1: ["X", "XX","XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"],
        2: ["C", "CC","CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"], 
        3: ["M", "MM", "MMM"]
    }
    reversed_n = ""
    for i in str(n): 
        reversed_n = i + reversed_n
        #j'initialise le valeur à 0
    digit = 0
    roman_numerals = ""
     #faire une boucle tant que le digit passé est inferieur a 0
    while digit < len(reversed_n):
        #boucle qui commencera de valeur i au resultat de longeur romain plus un et renvoie le resulats en entier
        for i in range(len(roman_numeral_dict[digit])+1): 
            #condition qui combine le resultat obtenu avec une nouvelle valeur
            if str(i+1) == reversed_n[digit]:
                roman_numerals = roman_numeral_dict[digit][i] + roman_numerals
               
        digit += 1
    return roman_numerals


