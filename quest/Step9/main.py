#fonction qui additionnant les 3 derniers nombres de la séquence pour générer le suivant.
def tribonacci(signature, n):
    #Creation d'un tableau qui stock les valeur du tribanotie
    tribonacci = []
    for i in range(n):
         #J'additionne les 3 valeur prrecedente que je stock dans la variable first
        first = signature[-1] + signature[-2] + signature[-3]
        #J'ajoute first a signature
        signature.append(first)
        #je stosck la nouvel valeur de signature dans elm
        elm = signature.pop(0)
        #j'ajoute valeur elm a tribonnaci 
        tribonacci.append(elm)
    #je retourne le tableau tribonnaci
    return tribonacci
